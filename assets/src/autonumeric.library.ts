import AutoNumeric from 'autonumeric';

@ViewChild('priceInputRef') priceInputRef : ElementRef;

<div class="col-sm-8">
  <input type="text" id="price" class="form-control"
    formControlName="price" #priceInputRef>
</div>

//? Custom Numeric Library properties
currencyMaskOptions: any =
{
  showOnlyNumbersOnFocus: true,
  decimalPlaces: 0,
  emptyInputBehavior: "null",
  maximumValue : "9999999999999999"
}

ngOnInit() {
  //? init for numeric
  new AutoNumeric(this.priceInputRef.nativeElement, this.currencyMaskOptions);

  this.route.params.subscribe(params => {
    this.id = params['id'];
    this.editMode = params['id'] != null;
  });
  this.initForm();
}

doUnformatedValue(val){
  return AutoNumeric.unformat(val, this.currencyMaskOptions);
}
