import { AutoNumeric } from 'autonumeric';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Product } from '../model/product.model';
import { ProductService } from '../service/product.service';
import { PageActionAccessor } from 'src/app/helper/page-action.accessor';
import { MatTableDataSource, MatPaginator, MatSort, MatDialog } from '@angular/material';
import { ModalProductComponent } from '../modal-product/modal-product.component';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {

  products: any;

  dataSource = this.products;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  displayedColumns = ['code', 'name', 'weight', 'price'];

  constructor(
    private productService: ProductService,
    private dialog: MatDialog
    ) {
    super();
  }

  ngOnInit() {
    this.fetchData();
  }

  fetchData() {
    this.productService.getProducts().subscribe(data => {
        this.products = data;
        this.dataSource = new MatTableDataSource(this.products);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
    },
      err => {
        console.error(err)
      }
    );
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  formatNumber(value) {
    return Number(value).toLocaleString('id-IDR', { minimumFractionDigits: 2,
      maximumFractionDigits: 2 })
  }

  openDialog(event){
    const dialogRef = this.dialog.open(ModalProductComponent, {
      data: event
    });

    dialogRef.afterClosed().subscribe(result => {
      this.fetchData();
    });
  }
}
