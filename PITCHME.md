---?image=assets/img/bg_logo.png
@title[Common UX Component]

@snap[east span-60 text-white]
## @color[white](UX Component)
@snapend

@snap[south docslink span-50]
[Angular Material](https://material.angular.io)
@snapend

---?code=assets/src/step-angular-material.script&lang=typescript
@title[Using Material Design]

@[1](Go to your angular project directory)
@[3-7](Install Angular Material, Angular CDK and Angular Animations)
@[3-7](Run _ng serve_ command, open your browser and hit the url - http://localhost:4200/ and the angular app is up.)

@@snap[north-east template-note text-black]
UX Component - Angular Material
@snapend

---?code=assets/src/product-list.to.datatable.ts&lang=typescript
@title[Datatable]

@[16](Modify products to _any_ type)
@[18-21](Add some varible for datatable binding)
@[6](Import from _'@angular/material'_)
@[34-45](Modify fetchData function)
@[47-49](Add additional function _applyFilter_)

@snap[north-east template-note text-black]
UX Component - Datatable | _product-list.component.ts_
@snapend

---?code=assets/src/product-list.to.datatable.html&lang=html
@title[Datatable]

@[1-14](Add header form content)
@[15](Binding dataSource)
@[17-39](Add templete for columns content)
@[40-50](Footer form content)

@snap[north-east template-note text-black]
UX Component - Datatable | _product-list.component.html_
@snapend

---?code=assets/src/product-list.to.datatable.css&lang=css
@title[Datatable]

@[1-17](Add costum css for datatable product at _product-list.component.css_)

@snap[north-east template-note text-black]
UX Component - Datatable | _product-list.component.css_
@snapend

---?color=lavender
@title[Import Material On Module]

```typescript
imports: [
  ...
  BrowserAnimationsModule,
  MatInputModule,
  MatTableModule,
  MatPaginatorModule,
  MatSortModule,
  MatProgressSpinnerModule,
  MatIconModule,
  MatButtonModule,
  MatCardModule,
  MatFormFieldModule,
],
```

@[3](Import BrowserAnimationsModule library from _'@angular/platform-browser/animations'_)
@[1-2,4-13](Import some Material library from _'@angular/material'_)

@snap[north-east template-note text-black]
UX Component - Import Material on Module | _app.module.ts_
@snapend

---?color=lavender
@title[Modal]

```typescript
cd src/app/product

ng g c modal-product --spec=false
```

@[1-3](Make modal-product component under product directory)

@snap[north-east template-note text-black]
UX Component - Adding Modal
@snapend

---?code=assets/src/product-list.to.datatable.ts&lang=typescript
@title[Modal]

@[25](Inject MatDialog as private parameter on constructor)
@[6](Import MatDialog from _'@angular/material'_)
@[56-64](Add openDialog function)

@snap[north-east template-note text-black]
UX Component - Modal | _product-list.component.ts_
@snapend

---?code=assets/src/product-list.to.datatable.html&lang=html
@title[Modal]

@[41-44](Modify action for openDialog with parameter row)

@snap[north-east template-note text-black]
UX Component - Modal | _product-list.component.html_
@snapend

---?color=lavender
@title[Import Material On Module]

```typescript
imports: [
  ...
  MatDialogModule
],

entryComponents: [
  ModalProductComponent
]
```

@[1-4](Import MatDialogModule from _'@angular/material'_)
@[6-8](Add entryComponents)

@snap[north-east template-note text-black]
UX Component - Import Material on Module | _app.module.ts_
@snapend

---?code=assets/src/modal-product.ts&lang=typescript
@title[Modal Product Component]

@[14-19](Create some global init variable)
@[21-26](Inject MatDialogRef<ModalProductComponent> from _'@angular/material'_)
@[21-26](Inject data dialog modal using @Inject(MAT_DIALOG_DATA) with type Product)
@[21-26](Inject ProductService & FormBuilder)
@[28-37](Fill some logic in ngOnInit function for initialize formGroup)
@[39-47](Create function fetchData)
@[49-57](Create function onFormSubmit for handle update data)
@[59-61](Create function for close modal)

@snap[north-east template-note text-black]
UX Component - Modal Product Component | _modal-product.component.ts_
@snapend

---?code=assets/src/modal-product.html&lang=html
@title[Modal Product Component]

@[1-19](Create some templete modal product html)
@[20-37](Create some templete modal product html)

@snap[north-east template-note text-black]
UX Component - Modal Product Component | _modal-product.component.html_
@snapend

---?code=assets/src/modal-product.css&lang=css
@title[Modal Product Component]

@[1-17](Add costum css for modal product at _modal-product.component.css_)

@snap[north-east template-note text-black]
UX Component - Modal Product Component | _modal-product.component.css_
@snapend

---?image=assets/img/bg_logo.png
@title[Numeric Library]

@snap[north-east text-white]
Numeric Library
@snapend

@snap[east span-60]
@ul[spaced text-white]
- Auto Numeric
- ngx-currency
- DecimalPipelink (Material Section)
- etc
@ulend
@snapend

@snap[south docslink span-50]
[AutoNumeric](https://www.npmjs.com/package/autonumeric)
[ngx-currency](https://www.npmjs.com/package/ngx-currency)
@snapend

---?code=assets/src/autonumeric.library.ts&lang=typescript
@title[Numeric Library]

@[1](Import AutoNumeric from _'autonumeric'_)
@[3](Add decorator @ViewChild with type ElementRef)
@[5-8](Add template reference #priceInputRef on HTML input)
@[11-17](Init some variable options for AutoNumeric)
@[19-28](Initialization on function ngOnInit)
@[30-32](Create function for unformat or revert value to original value before submit)

@snap[north-east template-note text-black]
UX Component - Numeric Library
@snapend

---?image=assets/img/bg_logo.png
@title[THANKYOU]

@snap[east span-50 text-white]
## @color[white](THANK'S)
@snapend
